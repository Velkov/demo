const COLUMNS = [
  {
    id: 0,
    value: 'id',
  }, {
    id: 1,
    value: 'Name',
  }, {
    id: 2,
    value: 'Category',
  }, {
    id: 3,
    value: 'SubCategory',
  }, {
    id: 4,
    value: 'Edit',
  }, {
    id: 5,
    value: 'Delete',
  },
];

export default COLUMNS;
