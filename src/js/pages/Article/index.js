import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';

import { getPost } from 'redux/reducers/posts';
import Button from 'components/Button';
import Fetching from 'components/Fetching';
import Header from 'components/Header';

import css from 'style/pages/Main';

@connect((state, props) => ({
  posts: state.posts.get,
  postsId: props.match.params.id,
}),
{ getPost }
)
export default class Article extends Component {
  static propTypes = {
    posts: PropTypes.object,
    postsId: PropTypes.string,
    getPost: PropTypes.func,
  };

  componentDidMount() {
    this.props.getPost();
  }

  render() {
    const { postsId, posts: { data, loading, loaded } } = this.props;
    const article = data.find((item) => item.id === +postsId);

    return (
      <section className={ cx(css.section, css.section_gray) }>
        <Header title='Home' />
        <section className={ cx(css.wrap, css.wrap_medium) }>
          <ul className={ css.grid }>
            <li className={ css.grid__item }>
              <div className={ css.grid__ibox }>

                <Fetching isFetching={ loading } size={ 45 } thickness={ 8 } position='initial' />

                {loaded && <div>
                  <h3 className={ css.grid__title }>
                    { article.todo_title }
                  </h3>

                  <h4>{ article.todo_category }</h4>
                  <h5>{ article.todo_sub_category }</h5>

                  {article.todo_url && <div>
                    <img src={ `${ article.todo_url }` } alt='' />
                  </div>}

                  <p className={ css.offset }>{ article.todo_desc }</p>

                  <ul className={ css.buttonGroup }>
                    <li className={ css.buttonGroup__item }>
                      <Button typeButton='raiseLink' to='/' className={ css.todoList__link } primary={ true }>Back to home</Button>
                    </li>
                    <li className={ css.buttonGroup__item }>
                      <Button typeButton='raiseLink' to={ `/post/${ article.id }/edit` } className={ css.todoList__link } secondary={ true }>Edit Post</Button>
                    </li>
                  </ul>
                </div>}

              </div>
            </li>
          </ul>
        </section>
      </section>
    );
  }
}
