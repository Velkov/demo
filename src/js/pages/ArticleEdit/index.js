import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';

import { getPost, editPost } from 'redux/reducers/posts';
import Button from 'components/Button';
import Fetching from 'components/Fetching';
import Header from 'components/Header';
import Input from 'components/Form/Input';
import Select from 'components/Form/Select';

import CATEGORIES from 'config/categories';

import css from 'style/pages/Main';

@connect((state, props) => ({
  posts: state.posts.get,
  postsId: props.match.params.id,
}),
{ getPost, editPost }
)
export default class ArticleEdit extends Component {
  static propTypes = {
    posts: PropTypes.object,
    postsId: PropTypes.string,
    getPost: PropTypes.func,
    editPost: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      catalog: [],
    };
  }

  componentDidMount() {
    this.props.getPost();
  }

  componentWillReceiveProps(nextProps) {
    const { postsId, posts: { data, loaded } } = nextProps;

    if (loaded) {
      const article = data.find((item) => item.id === +postsId);
      const categories = CATEGORIES.find((item) => item.value === article.todo_category);
      const subCategories = categories && categories.catalog.find((item) => item.value === article.todo_sub_category);

      this.setState({
        todoTitle: article.todo_title,
        todoDesc: article.todo_desc,
        todoUrl: article.todo_url,
        selectedCategory: categories !== undefined ? categories.id : '',
        selectedSubCategory: subCategories !== undefined ? subCategories.id : '',
        catalog: categories !== undefined ? categories.catalog : '',
      });
    }
  }

  onChangeCategory = (event, index, value) => {
    this.setState({
      selectedCategory: value.id,
      selectedSubCategory: '',
      catalog: value.catalog,
    });
  }

  onChangeSubCategory = (event, index, value) => {
    this.setState({
      selectedSubCategory: value.id,
    });
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  handleSubmit = async (event) => {
    event.preventDefault();

    const { postsId } = this.props;
    const { selectedCategory, selectedSubCategory } = this.state;
    const categories = CATEGORIES.find((item) => item.id === selectedCategory);
    const subCategories = categories && categories.catalog.find((item) => item.id === selectedSubCategory);

    if (this.state.todoTitle !== '') {
      await this.props.editPost(
        +postsId,
        {
          todo_title: this.state.todoTitle.trim(),
          todo_desc: this.state.todoDesc.trim(),
          todo_url: this.state.todoUrl.trim(),
          todo_category: categories !== undefined ? categories.value : '',
          todo_sub_category: subCategories !== undefined ? subCategories.value : '',
        }
      );
    }

    this.props.getPost();
  }

  formReset = () => {
    this.setState({
      todoTitle: '',
      todoDesc: '',
      todoUrl: '',
      selectedCategory: '',
      selectedSubCategory: '',
    });
  }

  render() {
    const { postsId, posts: { loading } } = this.props;
    const { todoTitle, todoDesc, todoUrl, selectedCategory, selectedSubCategory, catalog } = this.state;

    return (
      <section className={ cx(css.section, css.section_gray) }>
        <Header title='Home' />
        <section className={ cx(css.wrap, css.wrap_medium) }>
          <ul className={ css.grid }>
            <li className={ css.grid__item }>
              <div className={ css.grid__ibox }>

                <Fetching isFetching={ loading } size={ 45 } thickness={ 8 }>
                  <form onSubmit={ this.handleSubmit }>
                    <Input label='Title *' name='todoTitle' value={ todoTitle } onChange={ (event) => this.handleChange(event) } />
                    <Input label='Description' name='todoDesc' value={ todoDesc } onChange={ (event) => this.handleChange(event) } multiLine={ true } />
                    <Input label='Image URL' name='todoUrl' value={ todoUrl } onChange={ (event) => this.handleChange(event) } />
                    <Select data={ CATEGORIES } label='Category' value={ selectedCategory } handleChange={ this.onChangeCategory } />
                    {selectedCategory && <Select data={ catalog } label='Subсategory' value={ selectedSubCategory } handleChange={ this.onChangeSubCategory } />}

                    <div className={ cx(css.buttonGroup, css.buttonGroup_between) }>
                      <ul className={ css.buttonGroup }>
                        <li className={ css.buttonGroup__item }>
                          <Button type='submit' primaryClass={ true } disabled={ todoTitle === '' }>Save</Button>
                        </li>
                        <li className={ css.buttonGroup__item }>
                          <Button typeButton='raiseLink' to={ `/post/${ +postsId }` } className={ css.todoList__link } secondary={ true }>View Post</Button>
                        </li>
                      </ul>

                      <ul className={ css.buttonGroup }>
                        <li className={ css.buttonGroup__item }>
                          <Button typeButton='flat' onClick={ this.formReset } secondary={ true }>Reset</Button>
                        </li>
                        <li className={ css.buttonGroup__item }>
                          <Button typeButton='flatLink' to='/' className={ css.todoList__link }>Cancel</Button>
                        </li>
                      </ul>
                    </div>
                  </form>
                </Fetching>

              </div>
            </li>
          </ul>
        </section>
      </section>
    );
  }
}
