import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';

import { getCalc } from 'redux/reducers/calc';
import Button from 'components/Button';
import Header from 'components/Header';
import Fetching from 'components/Fetching';

import css from 'style/pages/Main';

@connect((state) => ({
  calc: state.calc.get,
}),
{ getCalc }
)
export default class CalcView extends Component {
  static propTypes = {
    calc: PropTypes.object,
    getCalc: PropTypes.func,
  }

  componentDidMount() {
    this.props.getCalc();
  }

  render() {
    const { data, loading } = this.props.calc;

    return (
      <section className={ cx(css.section, css.section_gray) }>
        <Header />

        <section className={ cx(css.wrap, css.wrap_medium) }>
          <ul className={ css.grid }>
            <li className={ cx(css.grid__item, css.grid__item_12) }>
              <div className={ css.grid__ibox }>
                <h3 className={ css.grid__title }>Calc View</h3>

                <Fetching isFetching={ loading } size={ 45 } thickness={ 8 }>
                  <p>Валюта: <b>{ data.currency }</b></p>
                  <p>Сумма: <b>{ data.amount }</b></p>
                  <p>Срок: <b>{ data.term } мес</b></p>
                  <p>Выплата процентов: <b>{ data.payment }</b></p>
                  <p>Процентная ставка: <b>{ data.rate }%</b></p>
                  <hr />
                  <p><b>Ваш пасивный доход:</b></p><br />
                  <p>Ежемесячно: <b>{ data.monthlyIncome }{ data.currencyCode }</b></p>
                  <p>За весь срок вложения: <b>{ data.fullTimeIncome }{ data.currencyCode }</b></p>
                  <hr />
                  <br />
                </Fetching>

                <br />
                <Button typeButton='raiseLink' to='/calc' primaryClass={ true }>Back to calc page</Button>
              </div>
            </li>
          </ul>
        </section>
      </section>
    );
  }
}
