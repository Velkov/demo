import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

import reducer from './reducers';
import apiMiddleWare from './middleware/apiMiddleware';

const initialState = {};
const defaultMiddleware = [thunk, apiMiddleWare];

const store = createStore(
  reducer,
  initialState,
  compose(
    applyMiddleware(...defaultMiddleware, logger),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
);

export default store;
