const GET_CALC = 'calc/GET_CALC';
const GET_CALC_SUCCESS = 'todo/GET_CALC_SUCCESS';

const EDIT_CALC = 'calc/EDIT_CALC';
const EDIT_CALC_SUCCESS = 'calc/EDIT_CALC_SUCCESS';

const initialState = {
  get: {
    loaded: false,
    loading: false,
    data: [],
  },
  edit: {
    loaded: false,
    loading: false,
    status: '',
  },
};

export default function postReducer(state = initialState, action) {
  switch (action.type) {
    case GET_CALC:
      return {
        ...state,
        get: {
          ...state.get,
          loading: true,
          loaded: false,
        },
      };
    case GET_CALC_SUCCESS:
      return {
        ...state,
        get: {
          ...state.get,
          loading: false,
          loaded: true,
          data: action.payload.result,
        },
      };
    case EDIT_CALC:
      return {
        ...state,
        edit: {
          loading: true,
          loaded: false,
          status: '',
        },
      };
    case EDIT_CALC_SUCCESS:
      return {
        ...state,
        edit: {
          loading: false,
          loaded: true,
          status: 'Success',
        },
      };
    default:
      return state;
  }
}

export const getCalc = () => {
  return {
    types: [GET_CALC, GET_CALC_SUCCESS],
    request: {
      method: 'GET',
      url: '/api/calc',
    },
  };
};

export const updatedCalc = (data) => {
  return {
    types: [EDIT_CALC, EDIT_CALC_SUCCESS],
    request: {
      method: 'PUT',
      url: '/api/calc',
      body: data,
    },
  };
};

