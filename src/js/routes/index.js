import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import history from 'routes/history';

import App from 'containers/App';

import Main from 'pages/Main';
import Jsonview from 'pages/Jsonview';
import Calc from 'pages/Calc';
import CalcView from 'pages/CalcView';
import Article from 'pages/Article';
import ArticleEdit from 'pages/ArticleEdit';
import Login from 'pages/Login';
import NotFound from 'pages/NotFound';

export default class Routes extends Component {
  render() {
    return (
      <Router history={ history }>
        <App>
          <Switch>
            <Route exact path='/' render={ (props) => <Main { ...props } /> } />
            <Route path='/jsonview' render={ (props) => <Jsonview { ...props } /> } />
            <Route path='/login' render={ (props) => <Login { ...props } /> } />

            <Route exact path='/calc' render={ (props) => <Calc { ...props } /> } />
            <Route path='/calc/view' render={ (props) => <CalcView { ...props } /> } />

            <Route exact path='/post/:id' render={ (props) => <Article { ...props } /> } />
            <Route exact path='/post/:id/edit' render={ (props) => <ArticleEdit { ...props } /> } />

            <Route component={ NotFound } />
          </Switch>
        </App>
      </Router>
    );
  }
}
