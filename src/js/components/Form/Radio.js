import React from 'react';
import PropTypes from 'prop-types';

import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';

import css from 'style/components/Radio';

this.styles = {
  fontSize: 14,
};

const Radio = ({ data, name, defaultSelected, valueSelected, onChange, ...props }) => (
  <RadioButtonGroup className={ css.radioGroup } name={ name } defaultSelected={ defaultSelected } valueSelected={ valueSelected } onChange={ onChange }>
    {data.map((item) => {
      return (
        <RadioButton key={ item.value } className={ css.radioGroup__item } label={ item.value } value={ item.value } style={ this.styles } { ...props } />
      );
    })}
  </RadioButtonGroup>
);

Radio.propTypes = {
  data: PropTypes.any,
  valueSelected: PropTypes.any,
  name: PropTypes.string,
  defaultSelected: PropTypes.string,
  onChange: PropTypes.func,
};

export default Radio;
