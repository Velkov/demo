import React from 'react';
import PropTypes from 'prop-types';
import { SelectField, MenuItem } from 'material-ui';

this.styles = {
  fontSize: 14,
};

const Select = ({ label, data, value, handleChange, ...select }) => (
  <SelectField
    fullWidth={ true }
    floatingLabelText={ label }
    value={ value }
    onChange={ (e, index) => handleChange(e, index, data[index]) }
    { ...select }
  >
    {data.map((item) => {
      return (
        <MenuItem key={ item.id } primaryText={ item.value } value={ item.id } style={ this.styles } />
      );
    })}
  </SelectField>
);

Select.propTypes = {
  value: PropTypes.any,
  data: PropTypes.array,
  label: PropTypes.string,
  handleChange: PropTypes.func,
};

export default Select;
