import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import css from 'style/components/InputRange';

const InputRange = ({ label, name, min, max, step, propValue, value, onChange, ...input }) => (
  <div className={ css.inputRange }>
    <ul className={ css.inputRange__group }>
      <li className={ cx(css.inputRange__item, css.grid__item_2) }>
        <label className={ css.inputRange__label } htmlFor='range'>{ label }</label>
      </li>
      <li className={ cx(css.inputRange__item, css.grid__item_10) }>
        <input className={ css.inputRange__input } type='range' name={ name } min={ min } max={ max } step={ step } value={ value } onChange={ onChange } { ...input } />
        <div className={ css.inputRange__value }>
          <span>{ min } { propValue }</span>
          <b>{ value } { propValue }</b>
          <span>{ max } { propValue }</span>
        </div>
      </li>
    </ul>
  </div>
);

InputRange.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  propValue: PropTypes.string,
  min: PropTypes.number,
  max: PropTypes.number,
  step: PropTypes.number,
  value: PropTypes.any,
  onChange: PropTypes.func,
};

export default InputRange;
