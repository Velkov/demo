import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { NavLink } from 'react-router-dom';
import { RaisedButton, FlatButton, IconButton, FloatingActionButton } from 'material-ui';

import css from 'style/components/Button';

const Button = ({
  typeButton = 'raise',
  to,
  iconName,
  className,
  onClick,
  primaryClass,
  secondaryClass,
  disabledClass,
  children,
  ...button
}) => (
  <span>
    {typeButton === 'raise' &&
      <RaisedButton className={ className } label={ children } primary={ primaryClass } secondary={ secondaryClass } disabled={ disabledClass } onClick={ onClick } { ...button } />
    }
    {typeButton === 'raiseLink' &&
      <NavLink to={ to } className={ className }>
        <RaisedButton label={ children } primary={ primaryClass } secondary={ secondaryClass } disabled={ disabledClass } onClick={ onClick } { ...button } />
      </NavLink>
    }

    {typeButton === 'flat' &&
      <FlatButton className={ className } label={ children } primary={ primaryClass } secondary={ secondaryClass } disabled={ disabledClass } onClick={ onClick } { ...button } />
    }
    {typeButton === 'flatLink' &&
      <NavLink to={ to } className={ className }>
        <FlatButton label={ children } primary={ primaryClass } secondary={ secondaryClass } disabled={ disabledClass } onClick={ onClick } { ...button } />
      </NavLink>
    }

    {typeButton === 'icon' &&
      <IconButton className={ className } onClick={ onClick } disabled={ disabledClass }>
        {iconName ? <i className={ cx(css.materialIcons, css.materialIcons__gray) }>{ iconName }</i> : { ...children } }
      </IconButton>
    }
    {typeButton === 'iconLink' &&
      <NavLink to={ to } className={ className } disabled={ disabledClass }>
        <IconButton onClick={ onClick }>
          {iconName ? <i className={ cx(css.materialIcons, css.materialIcons__gray) }>{ iconName }</i> : { ...children } }
        </IconButton>
      </NavLink>
    }

    {typeButton === 'circle' &&
      <FloatingActionButton onClick={ onClick }>
        {iconName ? <i className={ cx(css.materialIcons, css.materialIcons__gray) }>{ iconName }</i> : { ...children } }
      </FloatingActionButton>
    }
  </span>
);

Button.propTypes = {
  typeButton: PropTypes.string,
  to: PropTypes.string,
  iconName: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
  primaryClass: PropTypes.bool,
  secondaryClass: PropTypes.bool,
  disabledClass: PropTypes.bool,
  children: PropTypes.any,
};

export default Button;
