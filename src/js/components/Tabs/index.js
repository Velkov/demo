import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Tabs, Tab } from 'material-ui/Tabs';

import css from 'style/components/Tabs';

export default class TabsExampleSimple extends Component {
  static propTypes = {
    title: PropTypes.any,
    children: PropTypes.any,
  };

  state = {
    activeTab: 0,
  };

  handleActive = (tab) => {
    const activeTab = tab.props['data-route'];
    this.setState({
      activeTab,
    });
  };

  render() {
    const { title, children } = this.props;
    const { activeTab } = this.state;

    return (
      <Tabs className={ css.tabs }>
        {title.map((item, index) => {
          return (
            <Tab key={ `id_${ item }` } className={ css.tabs__link } label={ item } data-route={ index } onActive={ this.handleActive }>
              <div className={ css.tabs__wrap }>
                <h2 className={ css.tabs__title }>{ item }</h2>
                <div className={ css.tabs__content }>
                  { children[activeTab] }
                </div>
              </div>
            </Tab>
          );
        })}
      </Tabs>
    );
  }
}
