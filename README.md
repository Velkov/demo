# todo-list demo

![alt text](http://i.piccy.info/i9/1fd157782971a0ec928cabefc8d5dde5/1530870806/140037/1249921/FireShot_Capture_11_Todo_list_http_localhost_3003_.png)
![alt text](http://i.piccy.info/i9/d2918953a75fd3df815ea6168669dbde/1530870877/96371/1249921/FireShot_Capture_12_Todo_list_http_localhost_3003_.png)
![alt text](http://i.piccy.info/i9/7a2daa73d5dc8dd6ff2ee7fdb401d9d9/1528450418/171748/1249921/add.png)
![alt text](http://i.piccy.info/i9/c06bda8ac5b94a84f9fed3d2bcd9db09/1528450448/95971/1249921/edit.png)
![alt text](http://i.piccy.info/i9/a50f5330965dc07c1a105b9cff2c2fce/1533055261/38456/1260166/calcPage.png)
![alt text](http://i.piccy.info/i9/1f708b45a7f99bcf5647eeae63403f51/1533044065/23844/1260166/Screenshot_1.png)


```
$ git clone https://gitlab.com/Velkov/demo.git
```

```
$ cd demo
```

```
$ npm install -g yarn
```

```
$ yarn install
```

For start server with mockups:
```
$ yarn start:mock
```

Visit `http://localhost:3003/` 

Build will be placed in the `build` folder.

```
$ yarn build
```